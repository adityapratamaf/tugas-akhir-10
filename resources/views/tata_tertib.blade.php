@extends('layouts.master')


@section('content')
    <div class="breadcrumb-area shadow dark text-center bg-fixed text-light"
        style="background-image: url(https://png.pngtree.com/background/20230516/original/pngtree-large-library-with-wooden-shelves-and-books-picture-image_2599239.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Tata Tertib Peminjaman</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="faq-area left-sidebar course-details-area default-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 faq-content">
                    <!-- Start Accordion -->
                    <div class="acd-items acd-arrow">
                        <div class="panel-group symb" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#ac1">
                                            Tata Tertib Peminjaman Buku
                                        </a>
                                    </h4>
                                </div>
                                <div id="ac1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>
                                            Tata Terbit Dalam Peminjaman harus hubungi kepada admin
                                        </p>
                                        <ul>
                                            <li>Saudara maupun Saudari harus menghubungi admin </li>
                                            <li>Telah Mengembalikan Semua Buku Yang Di Pinjam Sebelumnya</li>
                                            <li>Memberikan Buku Kepada Petugas Perpustakaan Untuk Proses Peminjaman</li>
                                            <li>Saudara maupun saudari harus menjaga buku yang dipinjam</li>
                                            <li>Dilarang Bagi Saudara maupun saudari Yang Meminjam Buku untuk Mencoret,
                                                Menyobek, atau
                                                Menghilangkan Buku Yang Di Pinjam</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#ac2">
                                            Tata Tertib Pengembalian / Mengembalikan Buku
                                        </a>
                                    </h4>
                                </div>
                                <div id="ac2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Tata Tertib bagi Saudara maupun Saudari dalam mengembalikan buku
                                        </p>
                                        <ul>
                                            <li>Membawa Buku yang akan di Kembalikan </li>
                                            <li>Buku Dalam Keadaan Baik dan Tidak di Coret - Coret</li>
                                            <li>Keterlambatan Pengembalian Akan Di Kenakan Denda Rp 1.000 Perhari</li>
                                            <li>Menghilangkan Buku Akan Di Denda Rp 50.000 PerBuku yang hilang</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#ac3">
                                            Tata Tertib Pendaftaraan Angoota Perpustakaan
                                        </a>
                                    </h4>
                                </div>
                                <div id="ac3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Tata Tertib Saudara/Saudari dalam mendaftar menjadi anggota
                                        </p>
                                        <ul>
                                            <li>Hubungi admin setempat</li>
                                            <li>Jika Sudah terdaftar maka bisa meminjam buku sesuai kebutuhan</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Yang mengerjakan Sem Wanadri -->
                    <!-- End Accordion -->
                </div>
            </div>
        </div>
    </div>
@endsection
