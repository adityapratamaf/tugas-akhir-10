@extends('layouts.master')

@section('content')
    <!-- yang buat Sem Wanadri -->

    <div class="banner-area auto-height shadow dark text-light content-top-heading bg-fixed text-normal text-center"
        style="background-image: url(https://p2.piqsels.com/preview/185/126/401/china-academy-of-tropical-agriculture-building-sky.jpg); height:100%;">

        <div class="item">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="content">
                            <h1>Website Perpustakaan </h1>
                            <br>

                            <form action="{{ route('home.cari') }}" method="get">
                                @csrf
                                <input type="text" placeholder="Cari Judul" class="form-control" name="buku">
                                <button type="submit">
                                    <i class="fa fa-search" style="color: white"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="breadcrumb-area shadow dark text-center bg-fixed text-light"
        style="background-image: url(https://p0.piqsels.com/preview/367/529/210/uhd-5k-4k-bookshelf.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Profile Perpustakaan</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="about-area default-padding">
        <div class="container">
            <div class="row">
                <div class="about-info">
                    <div class="col-md-6 thumb">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8SEBUTERMVFhUVFhgYEhcYGBYXGBoaFhYaGBcbGhgYHSosGhomGxYYITEhKCotLi4uGB8zODMsNygtLisBCgoKDg0OGxAQGi0lICYvLS0yLS8wLS0tMi8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAK8BIAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcEBQgDAgH/xABKEAABAwIDBQQDCwkGBwEAAAABAAIDBBEFEiEGBzFBURNhcYEiMpEUNUJSU3KSobHB0RUXIzNidJOysyRUc4PC0hY0RIKi0+E2/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAIDBAEF/8QAMBEAAgIBAgMGBgICAwAAAAAAAAECAxEEIRIxQRMUUWHB8CJxkaGx0YHhMlIFMzT/2gAMAwEAAhEDEQA/ALxREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAReU0zWC73Bo6kgD618R1cTjZr2OPQOBPsBQGQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIo1tFtvh1Eck0t5Pk2Avf520b5kKLu3z0Oawp6gjr+j+zMpqqbWUmQdkVzZZqKH4FvFwyqcGNlMbzo1soyX8HcCe691MFGUXF4aJJp8giiu0G32G0biySXNIOLIwXkeJGjT3E3WgZvjw8usYpwOtmH6sykqptZSZF2RXNlkoo9SbX0EtLJVRyh0cTS6SwOdturDrf7VrsM3k4ZUTMhifIXyODWAxuAuepPBc4Jb7PY7xLxJkihNbvPwqGWSJ75M8T3seBG4jMxxa6x56gre4ltHR08DZ55msY9ocy98zgRcZWjUm3IBHCSxtzO8S8Tcoq8O9/C81rT2+NkH2ZrqV4FtHR1rC+mla+3rDVrm/Oa6xC7KucVlpnFOL5Mhu/n3uh/e2f0ZlCdyQH5V/yJPtYpvv497of3pn9GZQjcqbYpc/IS/axaq//ADS/kzz/AO9e/E6ARQnGd52F07iwPdM4aERNzAEftmwPkSsGj3v4Y4gSNmjvzLA4D6BJ9gWZVTazwsvdkE8ZRYiLBwzE4KiMSQSNkYfhNN9eh6HuKzlWTCKJ7Q7wMNo3GOSUvkHFkYzkfOPBp7ibqN/noo7/APLT263j+zMrI1TksqLIOyK5stBFE9nd4GHVjxHHIWSHgyQZCfmng49wN+5SKvq2QxPlffLG0udYXNmi5056KDi08NElJNZRlIoZQby8LmlZFG+TPI4NZeNwF3Gw1PDVfG0O8vDqSR0RL5ZG6OEYBAPQuJAv4XspdnPOMM5xxxnJNkVd4bvdw2RwbI2WG5tmc0Ob55CbDyU+p52SMD2ODmuALXNIIIPAgjiFyUJR/wAkIyUuTPZERRJBERAEREAREQBaDbnFn0mHzzx+u1gDD0c9wYD5F1/Jb9arafCG1lJNTuNu0ZYHo4EOafJwB8l2OMrJx5xsc1YHhstbVxwNd6czzdziTyLnOPU2BPerSbuVgya1cmfqI25fo3v9aq/EsMrMPnAla+KRjrseLgEg6OY/mPBSSk3r4swAF8UluJfGLnxLSPsXpWqyWHW9jBW647TW5sRuhqhVsjdI11O65fM3QgDi3IfhHlxHHopJvT2kNBTRUVK4te9li65LmRN9EWJ1zOIIv3Fa/A98t3BtZAGt5yREm3jGePkfJQ/erXtnxN72PD2dnF2bhqMpjDhbuu4nzVcY2TsSs6FspQjBuHU0uz+A1NbN2VOzM7i4k2a0fGc7kPrKsFm5WfJc1cYf07Nxb9LN9yku5OgYzDe1A9KaV5cedmHI0eGhP/crDULtTNTaj0O10R4U2cwY9gldhkro5fR7Vjm5mG7JGH1he3hcHUaL63fe+lJ/jN+9XPvdoWSYVK5wF4ix7DzBzBpt4hxCpnd9760n+M371dCztK23z3/BVOHBYkve5ibXe+FZ+9VH9Z6zcA2fr8UkAZ6QiYxhkkNmRsaAGNGnIcGgd/esLa73wrP3qo/rPV+bs6GOLCqfILdoztHnq5+pJ8rDyCW29nWmuYhXx2NPkVhiu6OvijL43xzEaljczXEAfBzaE92ihWDYpPSVDZoSWyRnXlcX9Jjh8U2sQuriubt6VGyLFagMFg4h5H7T2hzvabnzUNPc5txmSvqUEpRJ1vaxJlTg1JUM0Es8b7dLwTXB7wbjyVQU9VJHmyOLc7Cx9ubXWzNPcbBTjFZCdmKS5vatcB4ZahaPd9hzKjEqeOQXZnzOHXIC8DwJaPK6spShB+Cb+xG3MprHVI2uze7Cvq4xI7JDG4XYZL5nDkQwcB42XztPu0rqOMyjJNG0XeY75mgcSWn4PeLrocBfhaDxWXvdmc+/2aO7Qxg5h2P2lmw+pbLGTkJAmZyey+unxhqQfxKtbertqaemjipX2kqWZ844tiI4joXcAe4qptsqBtPiFTEz1WSuyDo13pAeQNvJY2M1j5OwzfAgjjb81hcB9S1yrjOUZ+/IzRnKEZRMjZjZqqr5uzgaNNZHuNmtBPEnme7iVYTdyjsutYM1uAhNr+OfgoPs1tpW0Mbo6bswHuzOLmZnE2A434WHDvPVbj87WL/Gh/h//VyxXN/BjAg6Uvi5mh2r2WqsPlDJwCHaxSNvldbp0cNNFZeye1L6zB6uKY5poIHguPF7Cx2Unv0IPgDzVdbS7bVtfG2Op7Mta7M3KzKb2I434WKzN3MxDq5nJ1DOT4ty2/mPtUbYydWZrdEqpJWYhyZE4nlrg5pILSC0jQgg3BB63Ur2c3d4jWxiVjWxxu1a+VxGYdWtAJI7zYG+l1HMIpxJUQxu4SSxsPg94aftXV0bA0AAWAFgBwAGgC7qbnXhROaepTWWUDLuqxJtRFG4NdHI8B0sbrhg4kuBAI0B5WvbVXzRUjIY2RRgNYxoawDkALBZKLFZbKzGTXCqMOQREVRYEREAREQBERAF4VVSyJjpJHBrGNLnuPAAC5JXuo9trgL66jfAyQxkkEH4Lsuoa/nlv06DwXUk3ucecbGPgG1OHYkwtaWuNzmhlAzac8rvWBGtxdfdRsFhD+NHEPmAs/kIVBY1szX0b7TwvbY+i9oLmaHQh7dPsK+G7T4iG9mKuoA4Ze0f+K29261y29+Bl7x0nHczd4OD09HXvhpnFzA1pIJzFjnXuwnnbQ+a0UkDwxshByuLmtPUstceWYe1bbZ/ZWurpLQxOIJ9OVwIYL8SXnie4XKumr3eUz8MZQg2dFd0ctte0OrnEfFcSQW9LdArZXKvCby+pVGp2ZaWCI7mtrYYmuop3Bl3l8DnGzSXWzMJ5G4uOtyrkuuX9oNkq+jcWzwuy8pGgujPfmA08DYrAiraqwjZJLbgGNfJbpbKD9ShPTxsfHFlkL3BcMkWxvj2th7E0MLg6Rzmmcg3DGtNw0n4xIGnIDvCrrd976Un+M371tsJ3dVj6SepmjfGGROdDHlPayOAuPQtcDu4krH2FwarZiNK59NO1omaXOdFIABrxJbopR4IVuMX4/gi+OU4ya95NPtd74Vn71Uf1nrofYH3rpP8CP8AlVD7UYLWur6tzaaoIdUzlpEMhBBmeQQQ3UEK+dh4nMw2la9pa4QsDmuBBBtwIPAqnUNOuOCyhNTl76m/XOu9733m+bH/ACBdFKgd6uE1UmKSujp5ntLY7OZFI5pswc2iyhpXiz+GT1CbhsfGJf8A5ml/fnfyTqJ4DijqWqiqGC5ieHW6jg4ebSR5qb4jhVUdnKaMQTGQVrnOYI35w3LPqW2uBqNbcwtJspsPVVc0kUkc0NoXvje+N7W52uYGtJcOBzHhrp3LVCUUpZ8WZ5wk5Rx4IvrZ7aClrYhJTyB1wMzbjO09HN5Fee020lLQwmSd4vY5GAjO88g0ffwC5zxTA66jktNFJG4GweAcp+bI3Q8OqxqXDqqd9o4pZXk62a5x8zb6yqVpY8+Lb31yWPUS5cO584lWvqJ5JnevK9zyBrq9xNh7bBSPeBs4+i9yZho+nYHHl2rb9oPa4Kabu92j4pG1NcBmaQ6KG4NnDUOeRpcHg0ePcp/tXs7BX07oJtPhRvHFjhwcPbYjmCV2eoippR5I5GmTg882VLuqqMKkDqathgMpdmhklY05gQAWZjzB1A537la42Mwr+5U38Jn4Kh9othMRo3EPhdJHykjBe0jqQNW+a0zKyqaMrZJQ3gGh0gHsBUp09o+KEhG1wXDKJe+KUmzlNIyKaGka+Rwa1vZsJF+BdYeiO82WZjmz1DT0dVJT08MTzTStzMY1pyltyLgcLgexUXgmyGIVZ/Q07yDxe4FjNeeZ3HyurwiwWopsGlp5JXTyCCQCwJtdhsxvNwHAX1+xUWwUMYlktrm55zHBQmzf/O0v7xD/AFWrqxcx7PYHWtrKYupagATwkkwygACRpJJLdB3rpxS1bTksEdKmovIREWQ0hERAEREAREQBERAEREAWOaKK9+zZfrlbf22WQiA/AF+qIbQ7VPgmMUTGnLbMXX4kXsACORXnW7ZEQxujYM775r3IbY20txutMdJa0mlzMctfRFyi5bx57Pxxt47sma8mwtBuGgHqAAVEW7ZO9zlxYO1vYDW2vP6uCy9l9o31L3Rva0OAu0tvYgEAgg89VyWltjFya2R2OuplKMIvd8uf0+fkShFUWNb1546p7IYYzFG8s9LNndlNnG4Nm3tpofuXrtJvVkZI1tJEwtyMc4yXJu9odlAaRawI11uU7tbttzLXqK99+RbCKkPzuYh8nD9F/wDuT87mIfJw/Rf/ALl3ulvgvqR71X4/Zl3qFbT7UFpMVO7UaPeOXh95Wql22mqKVg7PsZHj9LY8v2RxFxrY6jh3rXYLhMlTJkZoB67uTR+PctWn0igu0u6dPVnm6zXub7HT7t9V+F6vpuTPYzGJZ2vZL6RZaz+oN9D3iylCwsMw+OnjEcYsBxPMnmT3rNWC6UZTbisI9TTQnCpRm8vx98/mF+BfqKsuCIiAKP7R7VUNC5gqnlpkDiy0b33y2vq1pt6w4qQKmd/362j+ZN/NGrKoKc1FkLJuMW0WVs5tVRV+f3K8v7O2e7Hstmvb1gL8Ct4qg3A/9X/lf61b6WwUJuKFc3KKbCIirJhERAEREAREQBERAEREAREQBERAR/GNl4qiTtC4tcQA61iDbhoedl81eylO+JkYLm9nfK4WJNzc5rjXXVSJFcr7UklJ7cjO9JS3JuK35+fvyIrU4HRQ03ZSyZQTcPcQDcC2gtwty7154I3DqUuc2oDnOFsxtoONgAFi7cYbPJI17GlzMoFgCSCDc6Drca9yjH5MqfkX/QcvQqqVtXxWPfd8vf4PJvudN3wUr4dk8S5Y8vmZ+K7G4PPUum90lge7M+NpblJJu6xIu25+3Syzse2Dw2rDZo5exa1jWEsyltmCzb34OAsPYtF+TKn5F/0HfgttBgdYaSS7SA5zTkIIJABBNvNdsoUMPtfLfHX6EqtZZNtOno3snzx8jBpd1lBMCYqx7wONmxmy0tbsrR009opXzZOJcGhocOYtxss6mqpIi7I4tLmlrraG19R9SycFwmSpkyM0A9d3Jo/HuV0anW3Oc8pe9/6Mtmrd8VXXDEnzx6fPq3yRpa/EYoAHSkgEgWaLuPWw7grY2SqaOSma6kcHRnifhZrC+ccnajRQbaPdMZpWvhqXWLgJBIAcreZjLQPokc+KztuqyPB8LbT0YyOlPZtcPW9X9JITzfawv1cOixam9X8MYPry/foepo9J3ZOUlv4+i8jK2s3m0dG90UYM8zdHNaQGNPRz9db8gDZQuTfPXX9GnpwOQPaE+3MPsUJ2ZwKevqWwQ8Tdz3Hg1oIzOPXiNOZICt6m3PYcI8r5J3Ptq8Oa2x7m5eHjdRlGiraW7L1O2zeOxjbP74YJHhlXCYbm2dpL2eLhYFo9qsyCZj2h7HBzXAFrgbgg6ggjiFztt9sPLhz2uDjJA82Y8ixDuOV4HOwvccbHgpRuV2neJHUUrrsLXPgv8Et1e0dxFzbqD1ULaYOHHXyJV2yUuCZZu0u0dLQxdpUPtfRjRq956Nbz8eA5qscR30Tl39npo2t5GRznOPk21vaVCNtcefXVskrj6AcWQi+jY2mzbePE95Ut2I3WmqhbUVb3xseA6NjLZ3NOoc4uByg9LXt0U4011x4rCLtnOWIGVhm+iYO/tNMxzeZiLmuHk8kO9oWv3vY5T1raKanfmaWzAg6Oabx3a4cipNjG5ulcwmlmkZIBoJCHsJ77AEeOvgqexOglp5nwzNyyRuyuHf3dQRYg9LKVSqlLihs10/jBCx2RjifUtTcD/wBX/lf61L9sNvqOgOQ3lmtcRMtp0zu+D4ce5Vtu0xr3FQ4hUaZmiIRg8C9xc1nlcg+AUMoqaoraoMbeSaZ/EniTqXOPIDUnuC46VO2Upcl+iSsca4pc/wCycVO+SvJPZwwMHIEPeR4nML+wLJw3fNUhw90U8b28zGXMcPpEg/UpLg26TD42D3QXzSfCOYsbfmGtby8SVrtrt09P2LpKHO2RgLuzc4ua+2pAJ1DumtlDj072wS4b1vkn2zW0dLXRdrTvvbR7To9h6Oby4ceB5LcrmXYDH3UVdFIHWje4MmHIscbXPzSc1+5dMhU31dnLHQtqs445P1ERUloREQBERAEREAReM8rWNLnEBoFyTwAWvw/HqadxZG+7hyIIvbja41UlCTTaWyIOyEWotrL5LKy/kaPeVtPLQ07DCB2kry0OcLhoAuTbmeFvNVn+c/FflW/w2qztvMRwnsxT4g/1rOa1oc57eIDvQBLeYvz1UVxHZPZ2CBk8lRL2cn6oteHF1uOVrWX059Oa10OtQXHBtvyM9qscvglhfMjf5zsW+VZ/Dat7svvMrD2wqWtkyxPfGQMpzMGgNvgn2ha/3Lsr8vVfRf8A+tbbAsU2Zpc+R0jzI0seZI5Heg71m2y2APNW2Ktxwq3n5FdbmpZc19TC2b3m1zquNtRkfHI4NLWtyluY6FpvrboeKxJN6uICoLwI+yDv1WX4N+Gbjmtz68lLNmML2fjDq+CS7YjqZHOtETw9Bwvfpe/cvnAsG2erql0tPdz2nO6I52N4+sI3AXbc8BprwUHKnib7N4S8Pf1JJW8KSms/M0W0+8yubVvZT5GRxuygOZdziOJdfhr05KQV+2Mk9NGGNMbnsBm6gkagd3etdtnSUElX2sTLyD13g+g5w0BDeBI69yxKXD55WyGGPOWMLrcL2Fw2/wAY8AFpqoqUI2TjjHj+X6Hm6rV2ysdNTznbb8L1f3P3CqTt52QhwaX3tfo0XdYc9FamG4fHBGGRiwHE8yep71zNSY/URVjKoH9JG+4abgWGhZbkCLg+J5q8MF3m4ZPGDJL2D/hMkBFj3OGjgqNbKybWP8fXzNf/AB9FdMX/ALePovImypff449vSjl2chHjmbf7lJ8d3rYfBYQk1DiRmyaNDb+kczuJtewHnZazepTx4hhsVbSESNiJc4jiI3D07jkWuDbjlr0WamLhOLksJm61qUWkzE3Bxs/tTtM/6Md+X0j9v2K31zLsHtQ7DqrtbF0bxkmaOJbcEEftA8PEjmrwp94WEPYHe6423F7Ou1w7i0jipamuXG3jZkaJx4Ejw3rxMdhM+e3o5HN+cHi1vbbzVHbGvc2uiLPW9O38J6le9Hb2OtaKalv2LXZpHkWzub6oAPwRx14kDpr8bmMAdNWGoc39FA1wv1keMoA8GlxPl1VtcXXS3Irm1O5Y6EApGgvYDwLmg+FxddaxNAaA3gAAPC2i5b2owZ9HVy07xYMccn7TCbsIvxFreYPRWlsJvPpzCyCucWSMAa2UglrwOGYj1XW48j9SaqDmlKO/9nNPJQbiy1SqH35RsGIsLbXdA0v8Q94F/IBWNi28vC4Yy5swmd8FkYJJPeTo0d5VEbSY1LW1L6iXQvOjRwa0aNaPAfeq9LXLj4mieosjw8KMqiJ/JlT0NRT3+jMtTRyyteHQl7Xi9iwkO4a2LdeCmuweCurMOxGFgu+0L4/nML3AeYu3zUW2fxeWiqo6iMelG43a64uCC1zT0uCR3LXF5ckveyM7W0W+X9nt+VMT+Wqvpzfin5UxP5aq+nL+KvbBt4uF1DA4ztid8JkvoEHpc6HxBXzi+8fCoGkicSu5Mi9Mk+PAeZWftZZx2fv6F/ZLGeP7nPIo5fk3/Rd+C6ziPojwH2KNbE7ZQYjGS30JW/rIibkdC0/Cb3qUKm+xzeGsYLaa1FZTzkIiLOXBERAEREAREQGr2goXTU742H0ja1+BLXA2PsUT2b2eqm1TJJGZGsJJJIueNgLePsVgL8sr69TOEHBdfbMl2jrttjZLOV9Nnlfcp/eTsXXzVrqinjMrJQ3QEBzC1obYhxGml796wsY3dYg2jp8g7R7O07WMOF2Z3Bwy3sCNNe/qrtsll2OqmkltsTemg234nOf/AANi390k/wDH/cn/AANi390k/wDH/cujUVvfZ+C+/wCyHc4eLKSwnd1iBoqjOBHI/szHEXC7uzcSc1tBx0v9S1ezuFy073SOJZJZzbAjQHR1yON+5W1ttVzRwAR3AcSJHDkLaDz691uahuCYRJUyZWaNHru5NH3+C2aaTcHbY1j9Y9pHl66TViopT4v34er5cxguESVMmVmjR67uTR956BWbh1BHBGI4xYDieZPMk8ymHUMcEYjjFgOPUnmT1KzFg1Opdz8ve7PR0Wijp45e8nzfovL8la7b7ro6p7p6RzYpnXL2H9W9x1JuBdjieJsQeirer3cYxG63uYv72OY4faukl+WUIamcVjmaJ0Qluc84XuuxWZwzxthbfV0jhoO5rbkn2K4tjtkYMPgdEwmQyG8zncHG1tG8ALadepKkll+rll87Fh8jsKow3RUW1u6Iue6Sge1oJuYXkhovxyOANh0aR5qHP3aYwDb3NfvD47fzLo5FKOqsSxzIy08G8lF4DugrZHg1b2Qx8w055D4WFh4knwVyYNhMFLC2GBgaxvAcyeZJ5uPMrYIq7LZT5lkK4w5EY2y2NpsQjAk9CRv6uVvrDuPxm93ssqjxLdRisbiI2xzN5OY8A272vtY+3xXQaKVd84LC5EZ1RnuznfD91uLSOAdE2IHi5726DwbclSXaHdLIIIGUZa+Ruc1D3uyZi7LltobAZTYd56q4rL9UnqbG8nFp4JFe7q9kqvD/AHR7pDP0mTJkdm9XNe+gtxC89uN2MVW909O4RTO1e0/q3nqbC7XHmRe/RWMir7WfFx53J9nHh4ehzhV7tMYjNvc+fvY9hH1kL1oN1+LynWFsXfI9oA+jmP1LomyWV3e5+RV3aGSCbC7uoaB/bPeZZ7EAj0WMB45RzJ6n2BTxEWeU5SeZF0YqKwgiIokgiIgCIiAIiIAiIgCIiAIiIDxnia9pa4BzToQRcHxC+KWkjjbljY1regFlkou56HMLOQiIuHQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA//Z"
                            alt="Thumb" class="text-center">
                    </div>
                    <div class="col-md-6 info">
                        <h5>Profile Perpustakaan</h5>
                        <h2>Selamat Datang Di Perpusatakaan kelompok 10</h2>
                        <p>
                            Perpustakaan yang dibuat oleh kelompok 10 dibuat dengan tujuan untuk mempermudah dalam menyimpan
                            sebuah data buku
                        </p>
                    </div>
                </div>
                <div class="seperator col-md-12">
                    <span class="border"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="banner-area text-light content-top-heading text-normal text-center"
        style="background-image: url(https://c4.wallpaperflare.com/wallpaper/479/101/113/germany-saxony-gorlitz-hall-historical-literature-wallpaper-preview.jpg);height:800px;background-size:100% 100%;">

        <div class="item">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="content">
                            {{-- <h1>Aplikasi Perpustakaan kelompok 10</h1> --}}
                            <form action="#" method="get">

                                <h1>Visi & Misi</h1>
                                <p>
                                    Memberikan kemudahan
                                </p>
                                <p>
                                    Menjadikan Buku Sebagai jendela ilmu bagi dunia
                                </p>
                                {{-- @csrf
                            <input type="text" placeholder="Cari Judul Buku" class="form-control" name="text">
                            <button type="submit">
                                <i class="fa fa-search" style="color: white"></i>
                            </button> --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="about-area default-padding">
            <div class="container">
                <div class="row">
                    <div class="about-info">
                        <div class="col-md-6 thumb">
                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8SEBUTERMVFhUVFhgYEhcYGBYXGBoaFhYaGBcbGhgYHSosGhomGxYYITEhKCotLi4uGB8zODMsNygtLisBCgoKDg0OGxAQGi0lICYvLS0yLS8wLS0tMi8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAK8BIAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcEBQgDAgH/xABKEAABAwIDBQQDCwkGBwEAAAABAAIDBBEFEiEGBzFBURNhcYEiMpEUNUJSU3KSobHB0RUXIzNidJOysyRUc4PC0hY0RIKi0+E2/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAIDBAEF/8QAMBEAAgIBAgMGBgICAwAAAAAAAAECAxEEIRIxQRMUUWHB8CJxkaGx0YHhMlIFMzT/2gAMAwEAAhEDEQA/ALxREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAReU0zWC73Bo6kgD618R1cTjZr2OPQOBPsBQGQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIo1tFtvh1Eck0t5Pk2Avf520b5kKLu3z0Oawp6gjr+j+zMpqqbWUmQdkVzZZqKH4FvFwyqcGNlMbzo1soyX8HcCe691MFGUXF4aJJp8giiu0G32G0biySXNIOLIwXkeJGjT3E3WgZvjw8usYpwOtmH6sykqptZSZF2RXNlkoo9SbX0EtLJVRyh0cTS6SwOdturDrf7VrsM3k4ZUTMhifIXyODWAxuAuepPBc4Jb7PY7xLxJkihNbvPwqGWSJ75M8T3seBG4jMxxa6x56gre4ltHR08DZ55msY9ocy98zgRcZWjUm3IBHCSxtzO8S8Tcoq8O9/C81rT2+NkH2ZrqV4FtHR1rC+mla+3rDVrm/Oa6xC7KucVlpnFOL5Mhu/n3uh/e2f0ZlCdyQH5V/yJPtYpvv497of3pn9GZQjcqbYpc/IS/axaq//ADS/kzz/AO9e/E6ARQnGd52F07iwPdM4aERNzAEftmwPkSsGj3v4Y4gSNmjvzLA4D6BJ9gWZVTazwsvdkE8ZRYiLBwzE4KiMSQSNkYfhNN9eh6HuKzlWTCKJ7Q7wMNo3GOSUvkHFkYzkfOPBp7ibqN/noo7/APLT263j+zMrI1TksqLIOyK5stBFE9nd4GHVjxHHIWSHgyQZCfmng49wN+5SKvq2QxPlffLG0udYXNmi5056KDi08NElJNZRlIoZQby8LmlZFG+TPI4NZeNwF3Gw1PDVfG0O8vDqSR0RL5ZG6OEYBAPQuJAv4XspdnPOMM5xxxnJNkVd4bvdw2RwbI2WG5tmc0Ob55CbDyU+p52SMD2ODmuALXNIIIPAgjiFyUJR/wAkIyUuTPZERRJBERAEREAREQBaDbnFn0mHzzx+u1gDD0c9wYD5F1/Jb9arafCG1lJNTuNu0ZYHo4EOafJwB8l2OMrJx5xsc1YHhstbVxwNd6czzdziTyLnOPU2BPerSbuVgya1cmfqI25fo3v9aq/EsMrMPnAla+KRjrseLgEg6OY/mPBSSk3r4swAF8UluJfGLnxLSPsXpWqyWHW9jBW647TW5sRuhqhVsjdI11O65fM3QgDi3IfhHlxHHopJvT2kNBTRUVK4te9li65LmRN9EWJ1zOIIv3Fa/A98t3BtZAGt5yREm3jGePkfJQ/erXtnxN72PD2dnF2bhqMpjDhbuu4nzVcY2TsSs6FspQjBuHU0uz+A1NbN2VOzM7i4k2a0fGc7kPrKsFm5WfJc1cYf07Nxb9LN9yku5OgYzDe1A9KaV5cedmHI0eGhP/crDULtTNTaj0O10R4U2cwY9gldhkro5fR7Vjm5mG7JGH1he3hcHUaL63fe+lJ/jN+9XPvdoWSYVK5wF4ix7DzBzBpt4hxCpnd9760n+M371dCztK23z3/BVOHBYkve5ibXe+FZ+9VH9Z6zcA2fr8UkAZ6QiYxhkkNmRsaAGNGnIcGgd/esLa73wrP3qo/rPV+bs6GOLCqfILdoztHnq5+pJ8rDyCW29nWmuYhXx2NPkVhiu6OvijL43xzEaljczXEAfBzaE92ihWDYpPSVDZoSWyRnXlcX9Jjh8U2sQuriubt6VGyLFagMFg4h5H7T2hzvabnzUNPc5txmSvqUEpRJ1vaxJlTg1JUM0Es8b7dLwTXB7wbjyVQU9VJHmyOLc7Cx9ubXWzNPcbBTjFZCdmKS5vatcB4ZahaPd9hzKjEqeOQXZnzOHXIC8DwJaPK6spShB+Cb+xG3MprHVI2uze7Cvq4xI7JDG4XYZL5nDkQwcB42XztPu0rqOMyjJNG0XeY75mgcSWn4PeLrocBfhaDxWXvdmc+/2aO7Qxg5h2P2lmw+pbLGTkJAmZyey+unxhqQfxKtbertqaemjipX2kqWZ844tiI4joXcAe4qptsqBtPiFTEz1WSuyDo13pAeQNvJY2M1j5OwzfAgjjb81hcB9S1yrjOUZ+/IzRnKEZRMjZjZqqr5uzgaNNZHuNmtBPEnme7iVYTdyjsutYM1uAhNr+OfgoPs1tpW0Mbo6bswHuzOLmZnE2A434WHDvPVbj87WL/Gh/h//VyxXN/BjAg6Uvi5mh2r2WqsPlDJwCHaxSNvldbp0cNNFZeye1L6zB6uKY5poIHguPF7Cx2Unv0IPgDzVdbS7bVtfG2Op7Mta7M3KzKb2I434WKzN3MxDq5nJ1DOT4ty2/mPtUbYydWZrdEqpJWYhyZE4nlrg5pILSC0jQgg3BB63Ur2c3d4jWxiVjWxxu1a+VxGYdWtAJI7zYG+l1HMIpxJUQxu4SSxsPg94aftXV0bA0AAWAFgBwAGgC7qbnXhROaepTWWUDLuqxJtRFG4NdHI8B0sbrhg4kuBAI0B5WvbVXzRUjIY2RRgNYxoawDkALBZKLFZbKzGTXCqMOQREVRYEREAREQBERAF4VVSyJjpJHBrGNLnuPAAC5JXuo9trgL66jfAyQxkkEH4Lsuoa/nlv06DwXUk3ucecbGPgG1OHYkwtaWuNzmhlAzac8rvWBGtxdfdRsFhD+NHEPmAs/kIVBY1szX0b7TwvbY+i9oLmaHQh7dPsK+G7T4iG9mKuoA4Ze0f+K29261y29+Bl7x0nHczd4OD09HXvhpnFzA1pIJzFjnXuwnnbQ+a0UkDwxshByuLmtPUstceWYe1bbZ/ZWurpLQxOIJ9OVwIYL8SXnie4XKumr3eUz8MZQg2dFd0ctte0OrnEfFcSQW9LdArZXKvCby+pVGp2ZaWCI7mtrYYmuop3Bl3l8DnGzSXWzMJ5G4uOtyrkuuX9oNkq+jcWzwuy8pGgujPfmA08DYrAiraqwjZJLbgGNfJbpbKD9ShPTxsfHFlkL3BcMkWxvj2th7E0MLg6Rzmmcg3DGtNw0n4xIGnIDvCrrd976Un+M371tsJ3dVj6SepmjfGGROdDHlPayOAuPQtcDu4krH2FwarZiNK59NO1omaXOdFIABrxJbopR4IVuMX4/gi+OU4ya95NPtd74Vn71Uf1nrofYH3rpP8CP8AlVD7UYLWur6tzaaoIdUzlpEMhBBmeQQQ3UEK+dh4nMw2la9pa4QsDmuBBBtwIPAqnUNOuOCyhNTl76m/XOu9733m+bH/ACBdFKgd6uE1UmKSujp5ntLY7OZFI5pswc2iyhpXiz+GT1CbhsfGJf8A5ml/fnfyTqJ4DijqWqiqGC5ieHW6jg4ebSR5qb4jhVUdnKaMQTGQVrnOYI35w3LPqW2uBqNbcwtJspsPVVc0kUkc0NoXvje+N7W52uYGtJcOBzHhrp3LVCUUpZ8WZ5wk5Rx4IvrZ7aClrYhJTyB1wMzbjO09HN5Fee020lLQwmSd4vY5GAjO88g0ffwC5zxTA66jktNFJG4GweAcp+bI3Q8OqxqXDqqd9o4pZXk62a5x8zb6yqVpY8+Lb31yWPUS5cO584lWvqJ5JnevK9zyBrq9xNh7bBSPeBs4+i9yZho+nYHHl2rb9oPa4Kabu92j4pG1NcBmaQ6KG4NnDUOeRpcHg0ePcp/tXs7BX07oJtPhRvHFjhwcPbYjmCV2eoippR5I5GmTg882VLuqqMKkDqathgMpdmhklY05gQAWZjzB1A537la42Mwr+5U38Jn4Kh9othMRo3EPhdJHykjBe0jqQNW+a0zKyqaMrZJQ3gGh0gHsBUp09o+KEhG1wXDKJe+KUmzlNIyKaGka+Rwa1vZsJF+BdYeiO82WZjmz1DT0dVJT08MTzTStzMY1pyltyLgcLgexUXgmyGIVZ/Q07yDxe4FjNeeZ3HyurwiwWopsGlp5JXTyCCQCwJtdhsxvNwHAX1+xUWwUMYlktrm55zHBQmzf/O0v7xD/AFWrqxcx7PYHWtrKYupagATwkkwygACRpJJLdB3rpxS1bTksEdKmovIREWQ0hERAEREAREQBERAEREAWOaKK9+zZfrlbf22WQiA/AF+qIbQ7VPgmMUTGnLbMXX4kXsACORXnW7ZEQxujYM775r3IbY20txutMdJa0mlzMctfRFyi5bx57Pxxt47sma8mwtBuGgHqAAVEW7ZO9zlxYO1vYDW2vP6uCy9l9o31L3Rva0OAu0tvYgEAgg89VyWltjFya2R2OuplKMIvd8uf0+fkShFUWNb1546p7IYYzFG8s9LNndlNnG4Nm3tpofuXrtJvVkZI1tJEwtyMc4yXJu9odlAaRawI11uU7tbttzLXqK99+RbCKkPzuYh8nD9F/wDuT87mIfJw/Rf/ALl3ulvgvqR71X4/Zl3qFbT7UFpMVO7UaPeOXh95Wql22mqKVg7PsZHj9LY8v2RxFxrY6jh3rXYLhMlTJkZoB67uTR+PctWn0igu0u6dPVnm6zXub7HT7t9V+F6vpuTPYzGJZ2vZL6RZaz+oN9D3iylCwsMw+OnjEcYsBxPMnmT3rNWC6UZTbisI9TTQnCpRm8vx98/mF+BfqKsuCIiAKP7R7VUNC5gqnlpkDiy0b33y2vq1pt6w4qQKmd/362j+ZN/NGrKoKc1FkLJuMW0WVs5tVRV+f3K8v7O2e7Hstmvb1gL8Ct4qg3A/9X/lf61b6WwUJuKFc3KKbCIirJhERAEREAREQBERAEREAREQBERAR/GNl4qiTtC4tcQA61iDbhoedl81eylO+JkYLm9nfK4WJNzc5rjXXVSJFcr7UklJ7cjO9JS3JuK35+fvyIrU4HRQ03ZSyZQTcPcQDcC2gtwty7154I3DqUuc2oDnOFsxtoONgAFi7cYbPJI17GlzMoFgCSCDc6Drca9yjH5MqfkX/QcvQqqVtXxWPfd8vf4PJvudN3wUr4dk8S5Y8vmZ+K7G4PPUum90lge7M+NpblJJu6xIu25+3Syzse2Dw2rDZo5exa1jWEsyltmCzb34OAsPYtF+TKn5F/0HfgttBgdYaSS7SA5zTkIIJABBNvNdsoUMPtfLfHX6EqtZZNtOno3snzx8jBpd1lBMCYqx7wONmxmy0tbsrR009opXzZOJcGhocOYtxss6mqpIi7I4tLmlrraG19R9SycFwmSpkyM0A9d3Jo/HuV0anW3Oc8pe9/6Mtmrd8VXXDEnzx6fPq3yRpa/EYoAHSkgEgWaLuPWw7grY2SqaOSma6kcHRnifhZrC+ccnajRQbaPdMZpWvhqXWLgJBIAcreZjLQPokc+KztuqyPB8LbT0YyOlPZtcPW9X9JITzfawv1cOixam9X8MYPry/foepo9J3ZOUlv4+i8jK2s3m0dG90UYM8zdHNaQGNPRz9db8gDZQuTfPXX9GnpwOQPaE+3MPsUJ2ZwKevqWwQ8Tdz3Hg1oIzOPXiNOZICt6m3PYcI8r5J3Ptq8Oa2x7m5eHjdRlGiraW7L1O2zeOxjbP74YJHhlXCYbm2dpL2eLhYFo9qsyCZj2h7HBzXAFrgbgg6ggjiFztt9sPLhz2uDjJA82Y8ixDuOV4HOwvccbHgpRuV2neJHUUrrsLXPgv8Et1e0dxFzbqD1ULaYOHHXyJV2yUuCZZu0u0dLQxdpUPtfRjRq956Nbz8eA5qscR30Tl39npo2t5GRznOPk21vaVCNtcefXVskrj6AcWQi+jY2mzbePE95Ut2I3WmqhbUVb3xseA6NjLZ3NOoc4uByg9LXt0U4011x4rCLtnOWIGVhm+iYO/tNMxzeZiLmuHk8kO9oWv3vY5T1raKanfmaWzAg6Oabx3a4cipNjG5ulcwmlmkZIBoJCHsJ77AEeOvgqexOglp5nwzNyyRuyuHf3dQRYg9LKVSqlLihs10/jBCx2RjifUtTcD/wBX/lf61L9sNvqOgOQ3lmtcRMtp0zu+D4ce5Vtu0xr3FQ4hUaZmiIRg8C9xc1nlcg+AUMoqaoraoMbeSaZ/EniTqXOPIDUnuC46VO2Upcl+iSsca4pc/wCycVO+SvJPZwwMHIEPeR4nML+wLJw3fNUhw90U8b28zGXMcPpEg/UpLg26TD42D3QXzSfCOYsbfmGtby8SVrtrt09P2LpKHO2RgLuzc4ua+2pAJ1DumtlDj072wS4b1vkn2zW0dLXRdrTvvbR7To9h6Oby4ceB5LcrmXYDH3UVdFIHWje4MmHIscbXPzSc1+5dMhU31dnLHQtqs445P1ERUloREQBERAEREAReM8rWNLnEBoFyTwAWvw/HqadxZG+7hyIIvbja41UlCTTaWyIOyEWotrL5LKy/kaPeVtPLQ07DCB2kry0OcLhoAuTbmeFvNVn+c/FflW/w2qztvMRwnsxT4g/1rOa1oc57eIDvQBLeYvz1UVxHZPZ2CBk8lRL2cn6oteHF1uOVrWX059Oa10OtQXHBtvyM9qscvglhfMjf5zsW+VZ/Dat7svvMrD2wqWtkyxPfGQMpzMGgNvgn2ha/3Lsr8vVfRf8A+tbbAsU2Zpc+R0jzI0seZI5Heg71m2y2APNW2Ktxwq3n5FdbmpZc19TC2b3m1zquNtRkfHI4NLWtyluY6FpvrboeKxJN6uICoLwI+yDv1WX4N+Gbjmtz68lLNmML2fjDq+CS7YjqZHOtETw9Bwvfpe/cvnAsG2erql0tPdz2nO6I52N4+sI3AXbc8BprwUHKnib7N4S8Pf1JJW8KSms/M0W0+8yubVvZT5GRxuygOZdziOJdfhr05KQV+2Mk9NGGNMbnsBm6gkagd3etdtnSUElX2sTLyD13g+g5w0BDeBI69yxKXD55WyGGPOWMLrcL2Fw2/wAY8AFpqoqUI2TjjHj+X6Hm6rV2ysdNTznbb8L1f3P3CqTt52QhwaX3tfo0XdYc9FamG4fHBGGRiwHE8yep71zNSY/URVjKoH9JG+4abgWGhZbkCLg+J5q8MF3m4ZPGDJL2D/hMkBFj3OGjgqNbKybWP8fXzNf/AB9FdMX/ALePovImypff449vSjl2chHjmbf7lJ8d3rYfBYQk1DiRmyaNDb+kczuJtewHnZazepTx4hhsVbSESNiJc4jiI3D07jkWuDbjlr0WamLhOLksJm61qUWkzE3Bxs/tTtM/6Md+X0j9v2K31zLsHtQ7DqrtbF0bxkmaOJbcEEftA8PEjmrwp94WEPYHe6423F7Ou1w7i0jipamuXG3jZkaJx4Ejw3rxMdhM+e3o5HN+cHi1vbbzVHbGvc2uiLPW9O38J6le9Hb2OtaKalv2LXZpHkWzub6oAPwRx14kDpr8bmMAdNWGoc39FA1wv1keMoA8GlxPl1VtcXXS3Irm1O5Y6EApGgvYDwLmg+FxddaxNAaA3gAAPC2i5b2owZ9HVy07xYMccn7TCbsIvxFreYPRWlsJvPpzCyCucWSMAa2UglrwOGYj1XW48j9SaqDmlKO/9nNPJQbiy1SqH35RsGIsLbXdA0v8Q94F/IBWNi28vC4Yy5swmd8FkYJJPeTo0d5VEbSY1LW1L6iXQvOjRwa0aNaPAfeq9LXLj4mieosjw8KMqiJ/JlT0NRT3+jMtTRyyteHQl7Xi9iwkO4a2LdeCmuweCurMOxGFgu+0L4/nML3AeYu3zUW2fxeWiqo6iMelG43a64uCC1zT0uCR3LXF5ckveyM7W0W+X9nt+VMT+Wqvpzfin5UxP5aq+nL+KvbBt4uF1DA4ztid8JkvoEHpc6HxBXzi+8fCoGkicSu5Mi9Mk+PAeZWftZZx2fv6F/ZLGeP7nPIo5fk3/Rd+C6ziPojwH2KNbE7ZQYjGS30JW/rIibkdC0/Cb3qUKm+xzeGsYLaa1FZTzkIiLOXBERAEREAREQGr2goXTU742H0ja1+BLXA2PsUT2b2eqm1TJJGZGsJJJIueNgLePsVgL8sr69TOEHBdfbMl2jrttjZLOV9Nnlfcp/eTsXXzVrqinjMrJQ3QEBzC1obYhxGml796wsY3dYg2jp8g7R7O07WMOF2Z3Bwy3sCNNe/qrtsll2OqmkltsTemg234nOf/AANi390k/wDH/cn/AANi390k/wDH/cujUVvfZ+C+/wCyHc4eLKSwnd1iBoqjOBHI/szHEXC7uzcSc1tBx0v9S1ezuFy073SOJZJZzbAjQHR1yON+5W1ttVzRwAR3AcSJHDkLaDz691uahuCYRJUyZWaNHru5NH3+C2aaTcHbY1j9Y9pHl66TViopT4v34er5cxguESVMmVmjR67uTR956BWbh1BHBGI4xYDieZPMk8ymHUMcEYjjFgOPUnmT1KzFg1Opdz8ve7PR0Wijp45e8nzfovL8la7b7ro6p7p6RzYpnXL2H9W9x1JuBdjieJsQeirer3cYxG63uYv72OY4faukl+WUIamcVjmaJ0Qluc84XuuxWZwzxthbfV0jhoO5rbkn2K4tjtkYMPgdEwmQyG8zncHG1tG8ALadepKkll+rll87Fh8jsKow3RUW1u6Iue6Sge1oJuYXkhovxyOANh0aR5qHP3aYwDb3NfvD47fzLo5FKOqsSxzIy08G8lF4DugrZHg1b2Qx8w055D4WFh4knwVyYNhMFLC2GBgaxvAcyeZJ5uPMrYIq7LZT5lkK4w5EY2y2NpsQjAk9CRv6uVvrDuPxm93ssqjxLdRisbiI2xzN5OY8A272vtY+3xXQaKVd84LC5EZ1RnuznfD91uLSOAdE2IHi5726DwbclSXaHdLIIIGUZa+Ruc1D3uyZi7LltobAZTYd56q4rL9UnqbG8nFp4JFe7q9kqvD/AHR7pDP0mTJkdm9XNe+gtxC89uN2MVW909O4RTO1e0/q3nqbC7XHmRe/RWMir7WfFx53J9nHh4ehzhV7tMYjNvc+fvY9hH1kL1oN1+LynWFsXfI9oA+jmP1LomyWV3e5+RV3aGSCbC7uoaB/bPeZZ7EAj0WMB45RzJ6n2BTxEWeU5SeZF0YqKwgiIokgiIgCIiAIiIAiIgCIiAIiIDxnia9pa4BzToQRcHxC+KWkjjbljY1regFlkou56HMLOQiIuHQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA//Z"
                                alt="Thumb" class="text-center">
                        </div>
                        <div class="col-md-6 info">
                            <h5>Profile Perpustakaan</h5>
                            <h2>Selamat Datang Di Perpustakaan SD Sanber 10</h2>
                            <p >
                                Perpustakaan yang dibuat oleh kelompok 10 dibuat dengan tujuan untuk manajemen dalam menyimpan
                                sebuah buku perpustakaan
                            </p>
                            <a href={{route('home.buku')}} class="button">Cari Buku</a>
                        </div>
                    </div>
                    <div class="seperator col-md-12">
                        <span class="border"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb-area shadow dark text-center bg-fixed text-light"
        style="background-image: url(https://png.pngtree.com/background/20230516/original/pngtree-large-library-with-wooden-shelves-and-books-picture-image_2599239.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Tata Tertib Peminjaman</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="faq-area left-sidebar course-details-area default-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 faq-content">
                    <!-- Start Accordion -->
                    <div class="acd-items acd-arrow">
                        <div class="panel-group symb" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#ac1">
                                            Tata Tertib Peminjaman Buku
                                        </a>
                                    </h4>
                                </div>
                                <div id="ac1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>
                                            Tata Terbit Dalam Peminjaman harus hubungi kepada admin
                                        </p>
                                        <ul>
                                            <li>Saudara maupun Saudari harus menghubungi admin </li>
                                            <li>Telah Mengembalikan Semua Buku Yang Di Pinjam Sebelumnya</li>
                                            <li>Memberikan Buku Kepada Petugas Perpustakaan Untuk Proses Peminjaman</li>
                                            <li>Saudara maupun saudari harus menjaga buku yang dipinjam</li>
                                            <li>Dilarang Bagi Saudara maupun saudari Yang Meminjam Buku untuk Mencoret,
                                                Menyobek, atau
                                                Menghilangkan Buku Yang Di Pinjam</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#ac2">
                                            Tata Tertib Pengembalian / Mengembalikan Buku
                                        </a>
                                    </h4>
                                </div>
                                <div id="ac2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Tata Tertib bagi Saudara maupun Saudari dalam mengembalikan buku
                                        </p>
                                        <ul>
                                            <li>Membawa Buku yang akan di Kembalikan </li>
                                            <li>Buku Dalam Keadaan Baik dan Tidak di Coret - Coret</li>
                                            <li>Keterlambatan Pengembalian Akan Di Kenakan Denda Rp 1.000 Perhari</li>
                                            <li>Menghilangkan Buku Akan Di Denda Rp 50.000 PerBuku yang hilang</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#ac3">
                                            Tata Tertib Pendaftaraan Angoota Perpustakaan
                                        </a>
                                    </h4>
                                </div>
                                <div id="ac3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Tata Tertib Saudara/Saudari dalam mendaftar menjadi anggota
                                        </p>
                                        <ul>
                                            <li>Hubungi admin setempat</li>
                                            <li>Jika Sudah terdaftar maka bisa meminjam buku sesuai kebutuhan</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Yang mengerjakan Sem Wanadri -->
                    <!-- End Accordion -->
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb-area shadow dark text-center bg-fixed text-light"
     style="background-image: url(https://png.pngtree.com/background/20230516/original/pngtree-large-library-with-wooden-shelves-and-books-picture-image_2599239.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about-info">

                    <h1>Visi & Misi</h1>
                    <p>
                        Memberikan kemudahan
                    </p>
                    <p>
                        Menjadikan Buku Sebagai jendela ilmu bagi dunia
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
