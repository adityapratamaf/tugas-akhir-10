@extends('layouts.master')

@section('content')
@section('content')

    <div class="banner-area text-light content-top-heading text-normal text-center"
        style="background-image: url(https://c4.wallpaperflare.com/wallpaper/479/101/113/germany-saxony-gorlitz-hall-historical-literature-wallpaper-preview.jpg);height:800px;background-size:100% 100%;">

        <div class="item">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="content">
                            {{-- <h1>Aplikasi Perpustakaan kelompok 10</h1> --}}
                            <form action="#" method="get">

                                <h1>Visi & Misi</h1>
                                <p>
                                    Memberikan kemudahan
                                </p>
                                <p>
                                    Menjadikan Buku Sebagai jendela ilmu bagi dunia
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@endsection
