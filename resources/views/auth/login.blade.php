<!DOCTYPE html>
<html lang="en">

<!-- yang mengerjakan sem wanadri -->

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>SI - Perpustakaan</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="https://sanbercode.com/assets/img/identity/logo@2x.jpg" type="image/x-icon">

    <!-- Toggles CSS -->
    <link href="/theme/vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
    <link href="/theme/vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="/theme/dist/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- HK Wrapper -->
    <div class="hk-wrapper">

        <!-- Main Content -->
        <div class="hk-pg-wrapper hk-auth-wrapper"
            style="background-image: url('https://png.pngtree.com/background/20230516/original/pngtree-large-library-with-wooden-shelves-and-books-picture-image_2599239.jpg');background-size: cover;">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="auth-form-wrap pt-xl-0 pt-70">
                            <div class="auth-form w-xl-35 w-lg-65 w-sm-85 w-100 card pa-25 shadow-lg">

                                @if (session('fail'))
                                    <div class="alert alert-danger alert-wth-icon fade show" role="alert">
                                        <span class="alert-icon-wrap"><i class="zmdi zmdi-block"></i></span>
                                        @foreach (session('fail') as $field => $messages)
                                            @foreach ($messages as $message)
                                                <li>{{ $message }}</li>
                                            @endforeach
                                        @endforeach
                                    </div>
                                @endif

                                <a class="auth-brand text-center d-block mb-20" href="#">
                                    <img class="brand-img"
                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWwAAABWCAMAAADc11z0AAAAwFBMVEX///8AM2YlqeAIRX4/ZYu/y9gFN2nH0t15k65/mLJFc50WRHO5x9VHa5AMPG1yjqpavufI6feR1O/B0N7j6O4dSncvreHR7fj3+PrW3uYzW4SDob7P2OJkwunz9fjr7/MmUXxTdZeQpryLormwv8+escWarsI4X4equ8w4aZcWT4Vkg6Jae5x1kKwLUoWe2fEci8NrkLLe8vs/s+N2yeut3/NTfaVvk7QeYZBStN9qm7w4pNW24vWLqMNehqomlstw0oJMAAAJVElEQVR4nO2bfXubNheHRWLAFBvHDaRACG+2MS7OvMZJtz7P2n3/bzUkHQlJ4NRJHPe6Nv3+cGoJvd2Sjo4OLkJaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpa/049hL+6B/8dPV7sP/3qPvxXdHfRStM+iwjri7tXlR0bWNMT9+h1mpC+WL+6G8+KstawzyFg/UozomG/RIz1/HXFNewX6I2sNewXCFjfvJa1ho3mVzdXn9Buf3P3ED7u9x/bpOXm84cI3f52yYW/v5m1hh3eEIDExfiIP3fIx3g3q0tJm7ez1rA/EYQE857wfERLQvcPGfbvb2etYT8Qhjv88e2Rwr4mhuP2KNZZFB3f1nGws8i0o3Q4L4zMSIrM+ImZZMf3wLdZeZP0xRCqtuvFMvGPr+s1wsv5Md23n0/z1qTcfELhpqV7G33us774s1l2GMJy6rb99fJphcdbOVi+T/7gKUhXRe5Z+XTB5qODnVXT3DPceFsqo7NnAaXgbmtGdYErXLTtOWuyGvMFTffv1xZ51msmXQ1pXeRtt4JYTCSPz2Ja9XSJerDtxqMJxtp5wfJ5sea7drmG8/lDu8x3T4Tl9XXSArnm+gasf8Mjq6CcmRtclhMi+tWm+7Ns4Xs8d0ZniMEOx5ZUkmsyMgQFNU11ybcw6tob+Ri9JTybrxhRT0hcCsN0hKfjRIadrMV2je07r+9ndSWwhqHKnceJoSvCnqVTMXdKiALsLJdK5iY0E84MRQUpBhUHYgaKcuXZBj9qenLilJkYX8ZpVCJstZRhKZviPfR0d8V194S/P877rA3LbhO3KhjYhwB7rbBoOtijWC1aUtYjNb19NuWwRdaGaat8DKM1LktLTQwSytpVM+7pH5w56bdrrN6b9e5C0uOO3hZV1ga2I4uBDhLZQ12HQuNDhcjaboZyGg5b1KgPz1iHKOmxbm0/3obhup9BhL0Rvz9vbcZ7Gm6sKxn2nnz/eMX9kDCpZzB+m3cqbpytuIol2MGIQ7EyBbbb5RluKKyvoqzNesGWvzkE2+7geWu65OMUZUGXGHPucSiZvFjeV+1YWF3ueDWZVMzmr88L+xtxu/eyz5fEIyR0sKEnX7TlY+tgW2VGsuCrI8J2a5yX8TO0NQEwY3ECvVl6bNAcdlwlYVKtW3eCN4HXbVoHRp51W6PBqzK0GVUHZayZglSeCCawbYc9Bgd1BrnvbLbnMmziCYJuupgqxhvBWE2eyrrcwQ4YNbA4uQC74OcW2OkAreg/pp1vAmbZZ7A95ltUjOOaeQ3puP1XRifcqlkiHNFuWkIFLAct+fyxSbYEx6UgKdsTEH1OD7uPXDviCcK6vvif/OCCr0euUoXdTQQA9TlsYYumMOyIPuSJN5YGFiY8UvIMH+DJtxnogdOlwGapgXrV5bBOWswgjoWKsn5XzqHubGxsMZ1akVh6NpBhF11OQlNWDLYnurGwJRZ0GTeolzUF2MIMlT14WBRpIDAC9w5snGSFGwZ7wVaCILpxTnlEXv/40Ok2RcmXr62/U//4cY3C71//wK1LfogXF2OnorbBGxjsQoYtmrwcFg/AlogCyoJm5SNBbEpdtbUGQMmis30vJtGG2QIXc5ghhLo8sV04uZfoZEqUoF5Egk8k1Lf8gWMjft/nIz13MhT2efKdyWCLS2UKjMdsiaNe5iHfrDW5AFtgRWdG9RcGKgeSNEdeqWDg+/cFrhKdTHKc6fKSfN98JchJVGTJfL4PSieCyB/qfSLB9sQsejPcMtiJVI7eLtT7YCcP9RYate/yBgFLK1deS/VIWgNs6aIraYFOpkHYfxLYXwjsPfdDqkDuhTv5OWxXzFJgy+XGP4HtHoI9kwc0tAJEn1+BPQLYxcGGT7iyf2ZG/hZ8vnAlswDPSzZqy6Nhy+XoaAeu6qCgD5sWUUK16UDl0j1Xji65AHvw3kqknL9v0sEDcoIPyP8r/nVUOk4zkq/D8j6TD8jnYMvl6DwWkGX2ZPdhU0CyM8RstlS5ZLNNMQemxoLLZd5v1zxX6C+8Uliz9Jp23B0Yrez6DcAuGOxc9GBhpw+5fkw92BU0JT8W9zqV0kphjUi3FIfBBrP+PmTTv/B1pTUj37EzMbnttMQBiu94BDJraUz0OAv6q0i51DwHW0SawY0lgUU45Nz2YIPfpixt4CeYWjARJdgowXuKLAY7taBz76AHfO79Rc/HTbqR7Xb6AdtthfVSOomosQCjLdxOeMDtGNjdLY+FmWN2vubC+zD/nm6BvtsLk9351H7rhPjw2oYzhQ1gZRAKCPhMZjwaxWPFooWuThQXecIUrxAiDsd3xSOh32XWeOnl3HlNaS/ZuyuPLaMJd1iOgg0vDNKSTVHN/G1jzXy31PEgUtKHzQ4+CI6EC8+zu0AUvXqHzIO+RyHrLmzFVfcOqbve8DdG5vRULw/IO909SgnWLwrsH+Tzm2SvC8qOvHv0Heh1XXFqs8qsy4JTPBJ2O/Bi3HkgORLCytuyNlcONbSEdh92yOc23zrNGpdsFzQP7nmjWbNm8xiknY3DOfcjMX7NR9g+6Kwmk7IhVZ+GNonsPSJEDMi1Antyqfh80rUg6CLBPjroqh0LWxR59dMFDkVh2gO3Z7v/mqDlM1gDOYSefXkQDGVIvstrtdvfPLZzHX39vFmipWi0NzWabD7LrFHSj9yTAy7r9bCQXos9AzvuVQlWaujtjxsNwmYRWVFeNjSb1HJkvUsTWC2c6Q/dqAb9otOq7/MN9ISc3L7yJjFIpbfrz8CeKcvS5Vt20psGYpQH40K1+jLLxUt4pax4HgNPlWt54XewUdoLkFgnvD4e0pB/Lf9koO0/nCThTEwvfIg2/Bz2Atm5XJIrHUsIYxp8Gg7C+YXULfj5gS2mWlvBk6zEqh12B4LMiWRnrOYMF5oDdxl/2/XTG3e+WeYwaFO8OMckPBnZ5I/kt1YkyYG/rTUMK7D5+Vb9JU05hcbie5ZVkGLKFaZV1LCd4G27XH8BMaaR8gOgzGGvhe4jVmt35Z/MYDBuof5w6F106N7Yysa/SlrUttqNpF44Zf2qMLtv2wfKZZF57M/A7Lb9tltKaujbByrIEjM6+Gu10E8O/u7t5DrMWuvUmmvW59Ncsz6j7jTrM0r/R+lz6uFXd0BLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0vr36d/ACMVs1iqqGTjAAAAAElFTkSuQmCC"
                                        height="70" alt="brand" />
                                </a>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    @method('post')
                                    <h1 class="display-4 text-center mb-10">Sistem Informasi Administrasi Perpustakaan
                                    </h1>
                                    <p class="text-center mb-30"> Kelompok 10 </p>

                                    <div class="form-group">
                                        <input class="form-control" value="123456789" placeholder="Nomor Induk Pegawai"
                                            name="nip">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" value="123456789" placeholder="Password"
                                            name="password" type="password" value="" id="myInput">
                                    </div>
                                    <div class="custom-control custom-checkbox mb-25">
                                        <input class="custom-control-input" id="same-address" type="checkbox"
                                            onclick="myFunction()">
                                        <label class="custom-control-label font-14" for="same-address">Lihat
                                            Password</label>
                                    </div>
                                    <button class="btn btn-success btn-block" type="submit">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->

    <!-- JavaScript -->

    <!-- jQuery -->
    <script>
        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        window.setTimeout(function() {
            $(".alert").fadeTo(700, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 2500);
    </script>
    <script src="/theme/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/theme/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="/theme/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Jasny-bootstrap  JavaScript -->
    <script src="/theme/vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="/theme/dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="/theme/dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="/theme/dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="/theme/vendors/jquery-toggles/toggles.min.js"></script>
    <script src="/theme/dist/js/toggle-data.js"></script>

    <!-- Init JavaScript -->
    <script src="/theme/dist/js/init.js"></script>
</body>

</html>
