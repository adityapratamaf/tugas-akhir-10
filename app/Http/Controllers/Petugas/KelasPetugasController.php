<?php

namespace App\Http\Controllers\petugas;

use App\Http\Controllers\Controller;
use App\Models\Kelas;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class KelasPetugasController extends Controller
{
   
    public function index()
    {
        $kelas = Kelas::orderBy('jenis_kelas', 'ASC')->paginate(8);
        return view('petugas.kelas.kelas', compact(['kelas']));
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jenis_kelas'=> 'unique:kelas,jenis_kelas'
        ]);

        if ($validator->fails()) {
            return back()->with('fail', 'Kelas Telah Ada Mohon Cek Ulang');
        }

        Kelas::create([
            'jenis_kelas'=>$request->jenis_kelas,
        ]);

        return back()->with('success', 'Berhasil Menambahkan Kelas Baru');
    }


    public function update(Request $request, $id)
    {
        Kelas::find($id)->update([
            'jenis_kelas'=>$request->jenis_kelas
        ]);

        return back()->with('success', 'Berhasil Perbaharui Kelas');
    }

    
    public function destroy($id)
    {
        Kelas::find($id)->delete();

        return back()->with('success', 'Berhasil Menghapus Kelas');
    }
}
