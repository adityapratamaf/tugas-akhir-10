<?php

namespace App\Http\Controllers\petugas;

use App\Http\Controllers\Controller;
use App\Models\Rak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RakPetugasController extends Controller
{
    public function index()
    {
        $rak = Rak::orderBy('jenis_rak','ASC')->paginate(8);
        return view('petugas.rak.rak', compact(['rak']));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jenis_rak'=> 'unique:rak,jenis_rak'
        ]);

        if ($validator->fails()) {
            return back()->with('fail', 'Rak Buku Telah Ada Mohon Cek Ulang');
        }

        rak::create([
            'jenis_rak'=>$request->jenis_rak,
        ]);

        return back()->with('success', 'Berhasil Menambahkan Rak Buku Baru');
    }


  
    public function update(Request $request, $id)
    {
        Rak::find($id)->update([
            'jenis_rak'=>$request->jenis_rak
        ]);

        return back()->with('success', 'Berhasil Perbaharui Rak Buku');
    }

    public function destroy($id)
    {
        Rak::find($id)->delete();

        return back()->with('success', 'Berhasil Menghapus Rak Buku');
    }
}
