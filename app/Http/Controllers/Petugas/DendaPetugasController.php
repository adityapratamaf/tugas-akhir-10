<?php

namespace App\Http\Controllers\Petugas;

use App\Http\Controllers\Controller;
use App\Models\Denda;
use Illuminate\Http\Request;

class DendaPetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $denda = Denda::get();
        return view('petugas.denda.denda', compact(['denda']));
    }

    public function store(Request $request)
    {
        Denda::create([
            'nominal_denda' => $request->nominal_denda,
        ]);

        return back()->with('success', 'Berhasil Menambahkan Denda Peminjaman');
    }
    public function update(Request $request, $id)
    {
        Denda::find($id)->update([
            'nominal_denda' => $request->nominal_denda,
        ]);

        return back()->with('success', 'Berhasil Memperbaharui Denda Peminjaman');
    }

    public function destroy($id)
    {
        Denda::find($id)->delete();

        return back()->with('success', 'Berhasil Menghapus Denda Peminjaman');
    }
}
