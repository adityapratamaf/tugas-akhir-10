<?php

namespace App\Http\Controllers\Petugas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardPetugasController extends Controller
{
    public function index()
    {
        $siswa = DB::table('siswa')->get();
        $buku = DB::table('buku')->get();
        $pinjam = DB::table('riwayat_pinjam')->where('status', 'pinjam')->get();
        $kembali = DB::table('riwayat_pinjam')->where('status', 'selesai')->get();
        $aktivity = DB::table('riwayat_pinjam')
            ->join('siswa', 'siswa.id_siswa', '=', 'riwayat_pinjam.id_siswa')
            ->join('buku', 'buku.id_buku', '=', 'riwayat_pinjam.id_buku')
            ->select('riwayat_pinjam.*', 'siswa.nama_siswa', 'siswa.foto', 'buku.nama_buku')
            ->latest()
            ->limit(5)
            ->get();

        return view('petugas.dashboard.dashboard', compact('siswa', 'buku', 'pinjam', 'kembali', 'aktivity'));
    }
}
