<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $rules = [
            'nip' => 'required|numeric',
            'password' => 'required',
        ];

        $customeMessages = [
            'nip.required' => 'NIP wajib diisi',
            'nip.numeric' => 'NIP harus diisi dengan angka',
            'password.required' => 'Password wajib diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $customeMessages);

        if ($validator->fails()) {
            $failedMessages = $validator->errors()->messages();
            return back()->with('fail', $failedMessages)->withInput();
        }

        if (auth()->attempt(['nip' => $input['nip'], 'password' => $input['password']])) {
            // Authentication successful
            if (auth()->user()->type == 'Petugas') {
                return redirect()->route('petugas.dashboard');
            } else {
                return redirect()->route('home');
            }
        } else {
            // Authentication failed
            $validator->errors()->add('authentication', 'NIP dan Password Pengguna Salah');

            return back()->with('fail', $validator->errors()->messages())->withInput();
        }
    }
}
