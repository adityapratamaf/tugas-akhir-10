<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // $rules = [
        //     'name' => ['required', 'string', 'max:255'],
        //     'nip' => ['required', 'numeric', 'unique:users'],
        //     'password' => ['required', 'string', 'min:8', 'confirmed'],
        // ];

        $customMessages = [
            'name.required' => 'Nama wajib diisi',
            'name.max:255' => 'Nama tidak lebih dari 255 huruf',
            'nip.required' => 'NIP wajib diisi',
            'nip.numeric' => 'NIP harus diisi dengan angka',
            'nip.unique' => 'NIP sudah digunakan',
            'password.required' => 'Password wajib diisi',
            'password.min' => 'Password harus memiliki setidaknya :min karakter',
            'password.confirmed' => 'Konfirmasi password tidak cocok',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'nip' => ['required', 'numeric', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $customMessages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'nip' => $data['nip'],
            'password' => Hash::make($data['password']),
            'type' => 'Petugas'
        ]);
    }
}
