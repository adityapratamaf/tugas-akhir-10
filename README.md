## Final Project
Aplikasi Administrasi Perpustakaan

## Kelompok 10
 - Muhamad Risqi Khasani
 - Aditya Pratama Febriono  
 - Sem Wanadri 
  
## Tema Project
Sistem Administrasi Perpustakaan Berbasis Web

  

## ERD
<img  src='erd perpustakaan.png'>

## Tampilan
<img  src='SI-Perpustakaan.png'>

<img  src='SI-Perpustakaan-Web.png'>

## Link Video
Link Demo Aplikasi 
<a href="https://youtu.be/07TKYKxCEGQ">Link Video</a>
